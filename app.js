// const express = require('express');
// const app = express();
// const http = require('http').createServer(app);
// const io = require('socket.io')(http);

// app.get('/', function(req, res){
//   res.sendFile(__dirname + '/index.html');
// });

// io.on('connection', function(socket){
//   console.log('a user connected');

//   socket.on('disconnect', function(){
//     console.log('user disconnected');
//   });

//   socket.on('status update', function(status){
//     console.log('received status update:', status);
//     io.emit('status update', status);
//   });
//   socket.on('status updated', function(updates){
//      console.log("updated status", updates);
//      io.emit('status updated')
//   })
// });

// http.listen(3000, function(){
//   console.log('listening on *:3000');
// });


// /*
// >> approved
// >> rejected
// >> dispatched {
//   tracking id if wfh
// }
// first user page lo user ki kanapadali status -> controller and public page


// */


// Let's start with importing `NlpManager` from `node-nlp`. This will be responsible for training, saving, loading and processing.
const { NlpManager } = require("node-nlp");
console.log("Starting Chatbot ...");
// Creating new Instance of NlpManager class.
const manager = new NlpManager({ languages: ["en"] });
// Loading our saved model
manager.load();
// Loading a module readline, this will be able to take input from the terminal.
var readline = require("readline");
var rl = readline.createInterface(process.stdin, process.stdout);
console.log("Chatbot started!");
rl.setPrompt("> ");
rl.prompt();
rl.on("line", async function (line) {
    // Here Passing our input text to the manager to get response and display response answer.
    const response = await manager.process("en", line);
    console.log(response.answer);
    rl.prompt();
}).on("close", function () {
    process.exit(0);
});

rn9tAFduTQXj8rrR_lbx59uTnfWpdh7SGtWx4_UKH5I